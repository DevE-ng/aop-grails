package demo

import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration
import groovy.transform.CompileStatic
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.EnableLoadTimeWeaving
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver

@CompileStatic
//@EnableLoadTimeWeaving
class Application extends GrailsAutoConfiguration {

//    @Bean
//    public InstrumentationLoadTimeWeaver loadTimeWeaver() {
//        return new InstrumentationLoadTimeWeaver();
//    }

    static void main(String[] args) {
        GrailsApp.run(Application, args)
    }
}
