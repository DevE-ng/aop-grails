package demo

import grails.gorm.services.Service
import groovy.transform.CompileStatic

@CompileStatic
@Service()
class TestService {

    def serviceMethod() {
        "test ok"
    }
}
