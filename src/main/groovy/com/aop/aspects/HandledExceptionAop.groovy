package com.aop.aspects

import com.aop.annotations.FailOrSuccessOperationRegister
import com.aop.metrics.MetricTypes
import com.aop.metrics.MetricsCollection
import com.fasterxml.jackson.annotation.JsonTypeInfo
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.AfterReturning
import org.aspectj.lang.annotation.AfterThrowing
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.aspectj.lang.annotation.Pointcut
import org.aspectj.lang.reflect.MethodSignature
import org.springframework.stereotype.Component

import java.lang.reflect.Method
import java.util.concurrent.TimeoutException

//makes sure that the aspect is not a singleton, which would be the default.
//Instead, one aspect instance is created each time the application enters the control flow of myPointcut(),
// i.e. each time a method annotated by DetermineCaseTypeOfWork is being executed.
//@Aspect("percflow(anyExceptionPointcut())")
@Aspect
class HandledExceptionAop {
    private Boolean exceptionHappened;


    @Pointcut("@annotation(com.aop.annotations.FailOrSuccessOperationRegister) && execution(* *(..))")
    void anyExceptionPointcut() {}

    @AfterReturning(pointcut = "anyExceptionPointcut()", returning = "returnValue")
    void afterSuccessAdvice(JoinPoint jp, Object returnValue) {
        MethodSignature methodSignature = (MethodSignature) jp.getSignature();
        Method method = methodSignature.getMethod();
        FailOrSuccessOperationRegister annotation = method.getAnnotation(FailOrSuccessOperationRegister.class);

        if (!exceptionHappened) {
            MetricsCollection.ZeroOutFall(annotation.metricName() as String)
            System.out.println("Method executed without exception");
        } else {
            System.out.println("Exception has happened");
        }
    }

    @AfterThrowing(pointcut = "anyExceptionPointcut()", throwing = "e")
    void logExceptions(JoinPoint jp, Exception e) {
        System.out.println(">>>> not caught");
        System.out.println(e);
        System.out.println(">>>> not caught");
    }


    @Before("handler(*) && args(e)")
    void logCaughtException(
            JoinPoint thisJoinPoint,
            JoinPoint.EnclosingStaticPart thisEnclosingJoinPointStaticPart,
            Exception e
    ) {


        MethodSignature methodSignature = (MethodSignature) thisEnclosingJoinPointStaticPart.getSignature();
        Method method = methodSignature.getMethod();

        FailOrSuccessOperationRegister annotation = method.getAnnotation(FailOrSuccessOperationRegister.class);
        if (annotation.metricName() == MetricTypes.OblikRestAccess &&
                (e instanceof ArithmeticException ||
                        e instanceof TimeoutException)
        ) {
            MetricsCollection.IncreaseFalls(annotation.metricName() as String)
            exceptionHappened = true;
        }


        System.out.println("** ASPECT logCaughtException: ${e}, enclosingJP: ${thisEnclosingJoinPointStaticPart}. jp: ${thisJoinPoint}");


    }


}
