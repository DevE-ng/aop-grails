package com.aop.aspects

import org.aspectj.lang.JoinPoint
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.aspectj.lang.annotation.Pointcut
import org.springframework.stereotype.Component

@Aspect
class TestAspect {

    @Before("execution(* demo.TestService.*(..))")
    void justAroundService(JoinPoint jp) {

        System.out.println(">>>>TestService works<<<<<");

    }

    //@Before("execution(* com.aop.testpackage.TestStandalone.*(..))")
    @Before("execution(* demo.RoomController.index(..))")
    void justAround(JoinPoint jp) {

        System.out.println(">>>>room controller works<<<<<");

    }

//    @Before("execution(* demo.ExtraController.index(..))")
//    void justAroundExtra(JoinPoint jp) {
//
//        System.out.println(">>>>extra controller works<<<<<");
//
//    }
//
//    @Before("execution(* demo.BookingController.*(..))")
//    void justAroundController(JoinPoint jp) {
//
//        System.out.println(">>>>booking controller works<<<<<");
//
//    }
}
